\documentclass[aspectratio=1610, xcolor={dvipsnames}]{beamer}

\usepackage{amsmath,amsfonts,amsthm,bm}
\usepackage{animate}
\usepackage[backend=biber,
            style=alphabetic,
            firstinits=true]{biblatex}
\addbibresource{../../mendeley/library.bib}
\AtBeginBibliography{\scriptsize} % print bibliography smaller

\usepackage{booktabs}
\usepackage[english]{babel}
\usepackage{caption}
\usepackage{csquotes}
\usepackage{fontawesome}
\usepackage{graphicx}
\usepackage{hyperref}
\usepackage{romannum}
\usepackage{siunitx}
\usepackage{tikz}
\usetikzlibrary{arrows, arrows.meta, fit, calc, matrix,positioning, chains, decorations.pathreplacing}


% \usepackage{tikzscale}
\usepackage{standalone}

\usepackage{pgfplots}

\usepackage[sfdefault]{Fira Sans}
\usepackage[mathrm=sym]{unicode-math}
\usefonttheme{professionalfonts}
\setmathfont{Fira Math}

% ### Experimental ###

% \renewcommand{\cite}{} % this prevents cites when not loading bib


% ### End Experimental ###

% ### Theme ###

\usetheme{metropolis}
\metroset{subsectionpage=progressbar, numbering=fraction, titleformat=smallcaps}
\title{Journal Club}
\date{09.04.2021}
\author{Patrick Fuhlert}
\institute{Institute of Medical Systems Biology}
% Center for Molecular Neurobiology Hamburg\\
% University Medical Center Hamburg-Eppendorf}
\makeatletter
\input{metropolis_extensions.tex}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{document}

\sloppy

{
  \usebackgroundtemplate{
    \hspace*{4cm}\includegraphics[width=.9\textwidth]{img/title.png}
  }
  \maketitle
}

\begin{frame}{Content}
  \setbeamertemplate{section in toc}[sections numbered]
  \tableofcontents
\end{frame}

\section{Introduction}


\begin{frame}{Heart Failure\addtocounter{footnote}{-1}\footnotemark}
  \begin{figure}
    \includegraphics<1>[height=.7\textheight]{img/heart-failure.png}
  \end{figure}

  \footnotetext{\href{https://www.world-heart-federation.org/cvd-roadmaps/whf-global-roadmaps/heart-failure/}{www.world-heart-federation.org/cvd-roadmaps/whf-global-roadmaps/heart-failure/}}

  \begin{itemize}
    \item heart failure (HF) is a severe form of heart disease with multiple causes
    \item heart cannot pump enough blood to keep of with the patient's needs
    \item<2> high risk for severe damage, unplanned (re-) hospitalization and death
  \end{itemize}

  \begin{description}
    \item<2>[Motivation] Develop model for risk analysis of complex diagnosis\\ with multiple causes
  \end{description}
\end{frame}

\begin{frame}{Patient Data}
    \begin{description}[leftmargin=1em]
      \item[Incomplete] Any e.g.\ treatment outside of hospital?\\Is patient absent because he is healthy or dead?
      \item[Heterogen] Differences in individual patient documentation as well as patient history
      \item[Sparse] Tracking in inconsistent intervals (rather than continuous)\\Patient experiences a small subset of all possible medical events
      \item[Bias] The records for the same patient will most likely differ from doctor to doctor or hospital to hospital
    \end{description}

  \vspace*{2em}

  \begin{block}<2>{What to do with this type of data?}
    \alert{Survival Analysis} seems to be a good fit
  \end{block}

\end{frame}

\begin{frame}{Survival Analysis}
  \begin{itemize}
    \item Survival Curves: Estimate probability of surviving over time
    \item[\color{black!50!green}{+}] Can deal with incomplete data (censoring)
    \item[\color{black!50!green}{+}] Can predict a patient on \alert{individual level}
    \pause
    \item Estimators in this paper
    \begin{itemize}
      \item Cox Proportional Hazards (CoxPH) \cite{Cox1972} with Kalbfleisch-Prentice Extension \cite{Kalbfleisch1980}
      \item Multi-task Logistic Regression (MTLR) \cite{Yu2011} (from the author)
    \end{itemize}
  \end{itemize}
\end{frame}

\section{Methods}

\subsection{Data}

\begin{frame}{Alberta Health --- Sources}
  \begin{itemize}
    \item Discharge Abstract Database
  \item Ambulatory Care Database
  \item Practitioner Claims Database
  \item Alberta Health Care Insurance Plan Registry
  \end{itemize}
\end{frame}


\begin{frame}{Alberta Health -- Outline}

  \begin{minipage}{.5\textwidth}
    \begin{itemize}
      \item 41k primary HF hospitalizations of 30k patients
      \item 3015 medical codes
      \item Data used in this paper is 30.1\% right censored
      \item Goal: Predict  \alert{risk of heart failure rehospitalization or death}
    \end{itemize}
  \end{minipage}\begin{minipage}{.5\textwidth}
    \begin{figure}
      \includegraphics[width=.8\textheight]{img/alberta-censoring.png}
    \end{figure}
  \end{minipage}


\end{frame}

\subsection{Encoding}

\begin{frame}{International Classification of Diseases (ICD) \cite{Bramer1988}}
  \begin{minipage}{.55\textwidth}
    \begin{itemize}
      \item Codes are hierarchical
      \item This paper uses ICD-10-CM
      \item Codes from 3 to 7 characters
      \item 500 most frequent codes used as is
      \item Following codes truncated to 3 characters
      \item The models in this paper try to predict if any \alert{I50} is included in a visit
    \end{itemize}
  \end{minipage}\begin{minipage}{.44\textwidth}
    \begin{figure}
      \includegraphics[height=.4\textheight]{img/ICD-10-codes-heart.png}
      \caption*{ICD-10 codes related to heart failure\footnotemark}
    \end{figure}
  \end{minipage}
  \footnotetext{\cite{Katekao2013}}

\end{frame}

\begin{frame}{Data Representation --- Multi Hot Encoding}
  \begin{itemize}
    \item Encode ICD-codes as one hot vectors
    \item "sum" them up to aggregate one patient visit
    \item[\alert{-}] Length of vectors equals number of all codes (long and sparse)
    \item[\alert{-}] Distance between related codes should be smaller than distance of unrelated codes
    \item[\alert{-}] Does not represent that certain visits should be "closer together" than others
  \end{itemize}
\end{frame}

\begin{frame}{Data Representation --- Med2Vec}
  \begin{figure}
    \includegraphics[width=\textwidth]{img/choi-embedding.png}
    \caption*{Exemplary embedding by \cite{Choi2016}}
  \end{figure}
\end{frame}

\begin{frame}{Med2Vec --- Algorithm}
  \begin{itemize}
    \item Each medical code denoted as one hot vector that is combined to a multi-hot visit vector used as MLP input
    \item Transform multi-hot vector into latent representation
    \begin{itemize}
      \item 2-layer MLP followed by softmax-layer
      \item Objective functions:
      \begin{enumerate}
        \item Code-level representation puts medical codes that appear in the same visit closer together\footnote{Log-Likelihood to maximize codes that appear in the same visit}
        \item Visit-level representation puts visits of the same context closer together\footnote{Cross Entropy loss of visit representation to original visit and $n$ pre-/succeeding visits}
      \end{enumerate}
    \end{itemize}
    \item[\color{black!50!green}{+}] Exploits intra visit co-occurrence
    \item[\color{black!50!green}{+}] Exploits inter visit sequential information
  \end{itemize}
\end{frame}

\begin{frame}{Med2Vec --- Architecture}
  \begin{figure}[]
    \centering
    \includestandalone[height=.8\textheight]{tikz/med2vec-architecture}
    \caption*{Med2Vec neural network architecture by \cite{Choi2016}}
  \end{figure}
\end{frame}

\begin{frame}{Med2Vec --- Usage}
  \begin{itemize}
    \item Find data representation that puts related codes closer together
    \item Solution used in this paper: Med2Vec \cite{Choi2016}
    \item Trained on "early" and more general data with 230k all-cause hospitalizations (04-2002 -- 03-2008)
    \item Used data from (04-2008 -- 03-2016) for the models
  \end{itemize}

  \begin{figure}
    \centering
    \includestandalone[width=.8\textwidth]{tikz/embedding-train-test}
    \caption*{Timeline for learning and using embedding data}
  \end{figure}
\end{frame}

\subsection{Models}


\begin{frame}{Cox Proportional Hazards Model}
  \begin{itemize}
    \item Consider \(h(t) = 1 - S(t)\) to be called the hazard function
    \item Split hazard function in base hazard and influence of \alert{time independent} covariates
    \[h(t) = h_0(t) \cdot s\]
       \item Proportional Hazard Assumption:
    \item[] All individuals have the \alert{same time dependent hazard function}, but a \alert{unique scaling factor}
    \item Typically assumed that the hazard responds exponentially:
    \[h(t) = h_0(t) \cdot \underbrace{\exp(b_1 x_1 + b_2x_2 + \cdots + b_p x_p)}_{\text{Independent of } t}\]
    \hspace*\fill{\small---~\cite{Cox1972}}
    \item coefficients \(b_p\) measure the \alert{impact of covariates}
  \end{itemize}
\end{frame}

\begin{frame}{Multi-Task Logistic Regression \cite{Yu2011}}
  \begin{itemize}
    \item Discretize output to fixed times $[t_1, t_2, \ldots, t_m]$
    \item Train one multiple logistic regression classifier per bucket:
    \item[] $P(T \geq t_i) = (1 + \exp(\Theta_i \cdot x))^{-1}$
    \item Encode survival time y as sequence $y = (0,0,\ldots, 1, 1, 1)$ (no event yet at $t_i$)
    \item Discard patients as soon as they get censored
    \item Maximize each logistic regression individually
    \item Regularize by $|| \Theta_i ||_2$
  \end{itemize}
\end{frame}

\begin{frame}{Multi-Task Logistic Regression --- Visualization}
  \begin{figure}
    \includegraphics[width=1\textwidth]{img/mtlr.png}
    \caption*{left: KM-curve of population, middle: MTLR label encoding, right: individual survival predictions}
  \end{figure}
\end{frame}

\section{Evaluation}

\begin{frame}{Setup}

  \begin{itemize}
    \item Compare both models each with embedding and multi-hot encoding
    \item Evaluate discrimination and calibration
  \end{itemize}

\end{frame}

\subsection{Metrics}

\begin{frame}{Concordance Index}
  \begin{itemize}
    \item Population level metric by \cite{Harrell}
    \item Fraction of correct order of predictions for each point in time
    \item Independent of absolute values, only order matters
    \item Leave out a pair if the concordance is unknown due to censoring
    \item Perfect score is \(1\), random prediction is \(\approx 0.5\)
  \end{itemize}
  Since MTLR discretizes output, it only evaluates \alert{pairs in different time bins}
\end{frame}

\begin{frame}{Concordance Index --- Example}
  Suppose there are three survival probabilities at $t_1$: $S(t_1|x_1) > S(t_1|x_2) > S(t_1|x_3)$
  The concordance index rates the \alert{order} of our predictions:

  \begin{figure}
    \includestandalone[height=.6\textheight]{tikz/cindex123}
    \qquad
    \includestandalone[height=.6\textheight]{tikz/cindex312}
    \caption*{Concordance Index of 1 and .33}
  \end{figure}
\end{frame}

\begin{frame}{Calibration --- Visualization}
  \begin{figure}
    \includegraphics[width=.9\textwidth]{img/d-calibration.png}
    \caption*{Visualization of Calibration}
  \end{figure}
\end{frame}

\begin{frame}{Calibration}

  \begin{itemize}
    \item Divide patients in 10 bins according to prediction
    \item Apply Pearson chi-squared goodness-of-fit test to test if observed and expected proportions \alert{differ significantly} (high p-value)
  \end{itemize}
\end{frame}

\subsection{Results}

\begin{frame}{CoxPH-KP}
  \begin{figure}
    \includegraphics[width=.9\textwidth]{img/coxph-graph.png}
    \caption*{Sample patients predicted using CoxPH}
  \end{figure}
\end{frame}

\begin{frame}{MTLR}
  \begin{figure}
    \includegraphics[width=.9\textwidth]{img/mtlr-graph.png}
    \caption*{Sample patients predicted using MTLR}
  \end{figure}
\end{frame}

\begin{frame}{Concordance Index Over Time}
\begin{figure}
  \includegraphics[width=.95\textwidth]{img/cindex-plot.png}
  \caption*{\small Concordance index measured at multiple time points and its average}
\end{figure}
\end{frame}

\begin{frame}{Calibration Over Time}
  \begin{figure}
    \includegraphics[width=.75\textwidth]{img/calibration-plot.png}
    \caption*{\small Calibration measured using HL test at multiple time points for the 4 evaluated models. Note that we consider the model to be calibrated if the p-value is greater than the threshold p-value – here, we consider 0.05 and 0.001.}
  \end{figure}
\end{frame}

\begin{frame}{Embedded MTLR: Difference in Calibration Over Time}
  \begin{figure}
    \includegraphics[width=.8\textwidth]{img/emb-mtlr-calibration.png}
    \caption*{\small The bins of observed and expected probabilities associated with two calibration computations of the Emb-MTLR model applied for 2 year [left] and 7 year [right] time points. Note that left is well calibrated, while right is not calibrated, however both show good discrimination (concordance index $>$ 70\%)}
  \end{figure}
\end{frame}

\begin{frame}{Result Summary}
  Discrimination
  \begin{itemize}
    \item All models performed nearly similar
    \item Especially at later points in time
    \item Multi-hot models performed very well in time points $<$ 1 year
  \end{itemize}

  Calibration
  \begin{itemize}
    \item Embedded MTLR performed best
    \item Followed by embedded cox, so embedding helped to increase calibration
    \item Multi-hot models not well calibrated except for the first time point
  \end{itemize}
\end{frame}

\section{Conclusion}

\begin{frame}{Conclusion --- Theirs}
  \begin{itemize}
    \item Embedding vectors can be applied in a survival analysis setting
    \item Any variation or changes in ICD-10 codes or the usage by clinicians may have put noise on the input data (14 years)
    \item It is important to accurately define the time range of interest
    \item Drop in performance for later points in time for all models
    \item Since models performed similar regarding discrimination, calibration can be used as an additional measure
  \end{itemize}
\end{frame}

\begin{frame}{Conclusion --- Mine}
  \begin{itemize}
    \item Not at all revolutionary, but touched a few things I wanted to read about or try myself
    \item Embedding may improve (calibration) results
    \item "classic" statistical models still relevant in the field
    \item (time dependent) metrics for discrimination and calibration can be helpful to distinguish model performance
    \item[\alert{-}] long vs short term prediction. How to emphasize specific time periods? Not really answered
    \item[\color{black!50!green}{+}] Discretizing the output does not seem to hurt model performance
  \end{itemize}
\end{frame}

\backup

\begin{frame}[allowframebreaks]{References}
  \printbibliography[heading=none]
\end{frame}

\begin{frame}{Alberta Health --- Sources}
  \begin{itemize}
    \item Discharge Abstract Database
    \begin{onlyenv}<2>
    \begin{itemize}
      \item Records for each hospital visit
      \item admission and discharge date
      \item responsible diagnosis, "side"-diagnoses,
      \item intervention, "side"-interventions,
    \end{itemize}
  \end{onlyenv}
  \item Ambulatory Care Database
  \begin{onlyenv}<3>
    \begin{itemize}
      \item visits to hospital based physicians' offices
      \item visits to hospital's Emergency Departments
    \end{itemize}
  \end{onlyenv}
  \item Practitioner Claims Database
  \begin{onlyenv}<4>
    \begin{itemize}
      \item outpatient services (ambulant)
    \end{itemize}
  \end{onlyenv}
  \item Alberta Health Care Insurance Plan Registry
  \begin{onlyenv}<5>
    \begin{itemize}
      \item Tracks vital status of all Alberta residents
      \item includes demographic information
    \end{itemize}
  \end{onlyenv}
  \end{itemize}
\end{frame}

\begin{frame}{Calibration}
\begin{itemize}
  \item Compute the Hosmer-Lemeshow (HL) statistic \cite{Hosmer1980} using the actual vs estimated number over each decile\footnote{$O_{1g}$: observed events in decile, $E_{1g}$: estimated events in decile, $N$: total observations, $\pi_g$: Predicted risk in decile}
  \[ H = \sum_{g=1}^G \frac{(O_{1g} - E_{1g})^2}{N \pi_g (1 - \pi_g)}\]
  \item Apply Pearson chi-squared goodness-of-fit test to test if observed and expected proportions differ significantly
  \item Also, Brier Score \cite{Brier1951} (MSE of prediction vs outcome) as summarized performance measure
\end{itemize}
\end{frame}

\end{document}
